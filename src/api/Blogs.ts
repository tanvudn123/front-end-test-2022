import axiosClient from "./index";
import type { RequestBodyBlog } from "@/interface/Blogs";
export class Blogs {
  static handleFormData(blog: RequestBodyBlog) {
    const formData = new FormData();
    formData.append("blog[title]", blog.title);
    formData.append("blog[content]", blog.content);
    if (blog.image) {
      formData.append("blog[image]", blog.image);
    }
    return formData;
  }
  static getListBlogs(params: any) {
    const response = axiosClient.get(`/v2/blogs`, { params });
    return response;
  }
  static getBlogDetail(id: number) {
    const response = axiosClient.get(`/v2/blogs/${id}`);
    return response;
  }
  static createBlog(body: RequestBodyBlog) {
    const formData = this.handleFormData(body);
    const response = axiosClient.post(`/v2/blogs`, formData);
    return response;
  }
  static editBlog(id: number, body: RequestBodyBlog) {
    const formData = this.handleFormData(body);
    const response = axiosClient.put(`/v2/blogs/${id}`, formData);
    return response;
  }
  static deleteBlog(id: number) {
    const response = axiosClient.delete(`/v2/blogs/${id}`);
    return response;
  }
}
