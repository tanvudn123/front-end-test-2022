import axios, { type AxiosRequestConfig } from "axios";
import router from "@/router/index";

const axiosClient = axios.create({
  baseURL: "https://api-placeholder.herokuapp.com/api",
  headers: {
    "Content-Type": "application/json",
  },
});
axiosClient.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (
      (config.method === "post" || config.method === "put") &&
      config?.url?.includes("/v2/blogs") &&
      config.headers
    ) {
      config.headers["Content-Type"] = "multipart/form-data";
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosClient.interceptors.response.use(
  (response) => response,
  (error) => {
    //handler error
    if (error.request.status === 404) {
      router.push({ path: "/" });
    }
  }
);
export default axiosClient;
