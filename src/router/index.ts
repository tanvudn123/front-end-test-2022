import { createRouter, createWebHistory } from "vue-router";

const lazyLoadView = (view: string) => {
  return () => import(`@/views/${view}.vue`);
};
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "List Blog",
      component: lazyLoadView("BlogsList"),
    },
    {
      path: "/blog/detail/:id",
      name: "Blog Detail",
      component: lazyLoadView("BlogDetail"),
    },
  ],
});

export default router;
