export interface Blog {
  id: number;
  title: string;
  content: string;
  image: {
    url: string;
  };
  created_at: string;
  updated_at: string;
  comments_count: number;
}
export interface RequestBodyBlog {
  title: string;
  content: string;
  image: string;
}

export interface PaginationInfor {
  count: number;
  page: number;
  offset: number;
  total: number;
  prev: number | null;
  next: number | null;
}

export interface ParamSearchBlog {
  page: number;
  search?: string | undefined;
}
export interface Breadcrumb {
  text: string;
  disabled: boolean;
  href: string;
}
