import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type { Blog } from "@/interface/Blogs";
export const useBlogsStore = defineStore("blogs", () => {
  const blog = ref<Blog>();
  return { blog };
});
