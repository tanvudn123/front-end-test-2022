export const SORT_BY = [
  { value: "id", title: "id" },
  { value: "title", title: "Title" },
  { value: "content", title: "Content" },
  { value: "created_at", title: "Created_at" },
  { value: "updated_At", title: "Updated_At" },
];
export const SORT_DIRECTION = [
  { value: "asc", title: "Asc" },
  { value: "desc", title: "Desc" },
];
