export const cleanKeyEmpty = (object: any) => {
  for (const propName in object) {
    if (
      object[propName] === null ||
      object[propName] === undefined ||
      object[propName] === ""
    ) {
      delete object[propName];
    }
  }
  return object;
};
