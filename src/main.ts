import { createApp } from "vue";
import { createPinia } from "pinia";
import BootstrapVue3 from "bootstrap-vue-3";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-3/dist/bootstrap-vue-3.css";
import App from "./App.vue";
import router from "./router";
import VueLazyLoad from "vue3-lazyload";
import "./assets/main.css";
import Notifications from "@kyvg/vue3-notification";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(BootstrapVue3);
app.use(VueLazyLoad);
app.use(Notifications);
app.mount("#app");
